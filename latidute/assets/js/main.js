(function ($) {
    window.onload = function () {
        $(document).ready(function () {		
			fixed_header_main();
			back_to_top();
			slider_room_box();
			slider_rating_client();
			slider_blog();
			open_nav_header();
        });        
    };
})(jQuery);

function fixed_header_main(){
	var top_bar = document.querySelector('#header .top-bar');
	var header_main = document.querySelector('#header .header-main');
	var header = document.querySelector('#header');
	var check = true

	window.addEventListener('scroll', function(){
		if(window.pageYOffset > 200){
			if(check == true){
				header.classList.remove('has-transparent');
				top_bar.classList.add('off-top-bar');
				header_main.classList.add('fixed-main');
				check = false;
			}
		}
		else{
			if(check == false){
				header.classList.add('has-transparent');
				top_bar.classList.remove('off-top-bar');
				header_main.classList.remove('fixed-main');
				check = true;
			}
		}
	})
}

function back_to_top() {
	// browser window scroll (in pixels) after which the "back to top" link is shown
	var offset = 300,
		//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
		offset_opacity = 1200,
		//duration of the top scrolling animation (in ms)
		scroll_top_duration = 700,
		//grab the "back to top" link
		$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
		);
	});
}

function slider_room_box(){
	$('#home-page .type-of-room .owl-carousel').owlCarousel({
		loop:true,
		margin:25,
		nav:true,
		dots: false,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
			nav:true
		},
		576:{
		   items:2,
		   nav: true,
		},
		 768:{
		   items:2,
		   nav:true,
		},
		1024:{
			items:3,
		   nav:true,
		},

		1400:{
		   items:3,
		   nav:true,
		}
	 }
  })
}

function slider_rating_client(){
	$('#home-page .service .slide-rating .owl-carousel').owlCarousel({
		loop:true,
		margin: 25,
		nav: false,
		dots: true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},
		576:{
		   items:1,
		},
		768:{
		   items:1,
		},
		1024:{
			items:2,
		},

		1400:{
		   items:2,
		}
	 }
  })
}

function slider_blog(){
	$('#home-page .blog .owl-carousel').owlCarousel({
		loop:true,
		margin:30,
		nav:true,
		dots: false,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},
		576:{
			items:1,
			nav:true,
		},
		768:{
			items:2,
			nav:true,
		},
		1024:{
			items:3,
		},

		1400:{
			items:3,
		}
	 }
  })
}

function open_nav_header(){
	var btn_open_nav = document.querySelector('#btn-open-bars');
	var overlay = document.querySelector('.overlay-mb');
	var close_mb =document.querySelector('#close-nav-mb');
	var mb_nav = document.querySelector('.moblie-nav')
	btn_open_nav.addEventListener('click', function(){
		mb_nav.classList.add('active-mb');
		overlay.classList.add('active');
	})

	close_mb.addEventListener('click', function(){
		mb_nav.classList.remove('active-mb');
		overlay.classList.remove('active');
	})

	overlay.addEventListener('click', function(){
		mb_nav.classList.remove('active-mb');
		overlay.classList.remove('active');
	})

}

function click_show_pick_date(){
	var input = $('#home-page .search-form-banner .check-in span');
	if(input == null){
		return 0;
	}

	else{
		input.click(function(){
			input.before.css("display", "none");
		})
	}
}